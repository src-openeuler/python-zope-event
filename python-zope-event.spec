Name:           python-zope-event
Version:        5.0
Release:        2
Summary:        Zope Event Publication
License:        ZPLv2.1
URL:            http://pypi.python.org/pypi/zope.event/
Source0:        https://files.pythonhosted.org/packages/46/c2/427f1867bb96555d1d34342f1dd97f8c420966ab564d58d18469a1db8736/zope.event-5.0.tar.gz
Patch0:         Adapt-to-intersphinx_mapping-format-due-to-Sphinx-upgrade.patch
BuildArch:      noarch

%description
The zope.event package provides a simple event system. It provides
an event publishing system and a very simple event-dispatching system
on which more sophisticated event dispatching systems can be built.
(For example, a type-based event dispatching system that builds on
zope.event can be found in zope.component.)

%package -n python3-zope-event
Summary:        Zope Event Publication (Python 3)
%{?python_provide:%python_provide python3-zope-event}

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

BuildRequires:  python3-sphinx

Requires:       python3

%description -n python3-zope-event
The zope.event package provides a simple event system. It provides
an event publishing system and a very simple event-dispatching system
on which more sophisticated event dispatching systems can be built.
(For example, a type-based event dispatching system that builds on
zope.event can be found in zope.component.)

This package contains the version for Python 3.

%prep
%autosetup -n zope.event-%{version} -p1

%build
%py3_build

# build the sphinx documents
pushd docs
PYTHONPATH=../src make SPHINXBUILD=sphinx-build-3 html
rm -f _build/html/.buildinfo
popd

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-zope-event
%doc CHANGES.rst COPYRIGHT.txt LICENSE.txt README.rst
%doc docs/_build/html/
%license LICENSE.txt
%{python3_sitelib}/zope/event/
%exclude %{python3_sitelib}/zope/event/tests.py*
%exclude %{python3_sitelib}/zope/event/__pycache__/tests*
%dir %{python3_sitelib}/zope/
%{python3_sitelib}/zope.event-*.egg-info
%{python3_sitelib}/zope.event-*-nspkg.pth


%changelog
* Tue Jan 21 2025 wangkai <13474090681@163.com> - 5.0-2
- Fix build failure due to Sphinx upgrade to 8.1.3

* Mon Jul 10 2023 chenzixuan<chenzixuan@kylinos.cn> - 5.0-1
- Update package to version 5.0

* Thu Feb 9 2023 wubijie <wubijie@kylinos.cn> - 4.6-1
- Update package to version 4.6

* Tue Aug 02 2022 zhaoshuang <zhaoshuang@uniontech.com> - 4.5.0-1
- update to v4.5.0

* Thu Oct 29 2020 xinghe <xinghe1@huawei.com> - 4.2.0-14
- remove python2 dependency

* Tue Sep 8 2020 Ge Wang <wangge20@huawei.com> - 4.2.0-13
- Modify Source0 Url

* Wed Feb 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.2.0-12
- Package init
